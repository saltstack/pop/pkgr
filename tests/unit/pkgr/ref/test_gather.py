import pytest


@pytest.mark.parametrize(
    "version, expt_salt_version, expt_pkg_version",
    (
        ("3002", "3002", None),
        ("3002-1", "3002", "1"),
        ("3002.1-2", "3002.1", "2"),
        ("3003rc1-4", "3003rc1", "4"),
    ),
)
def test_ref_parsing(mock_hub, hub, version, expt_salt_version, expt_pkg_version):
    """
    Test gather() sets the correct salt and package version attributes
    """
    mock_hub.OPT.pkgr = {"ref": version}
    mock_hub.pkgr.ref.salt.gather = hub.pkgr.ref.salt.gather
    salt_version, pkg_version = mock_hub.pkgr.ref.salt.gather()
    assert salt_version == expt_salt_version
    assert pkg_version == expt_pkg_version
