import pathlib
from unittest import mock
from unittest.mock import call

import pytest
from dict_tools.data import NamespaceDict


@pytest.mark.parametrize(
    "repo, name",
    [
        ("https://github.com/saltstack/salt.git", "salt"),
        ("git@gitlab.com:saltstack/open/salt-pkg.git", "salt-pkg"),
    ],
)
def test_proj_name(mock_hub, hub, repo, name):
    """
    Test gather() sets the correct proj_name
    """
    mock_hub.OPT = NamespaceDict(
        pkgr={
            "git": repo,
            "ref": "",
            "timeout": 600,
            "patch_file": None,
            "sdir": "",
            "proj_name": "",
        }
    )

    mock_hub.pkgr.init.gather = hub.pkgr.init.gather
    mock_patch = mock.patch("subprocess.run", return_value=mock.MagicMock())
    mock_os = mock.patch("os.makedirs")
    with mock_patch, mock_os:
        mock_hub.pkgr.init.gather()
    assert mock_hub.pkgr.proj_name == name


@pytest.mark.parametrize(
    "patch,exp_ret",
    [
        ("tests/files/patch/3004.patch", False),
        ("/pkgr/tests/files/patch/3004.patch", True),
        ("../../", False),
    ],
)
def test_patch_file(mock_hub, hub, patch, exp_ret):
    """
    Test gather() when patch_file is set
    """
    mock_hub.OPT = NamespaceDict(
        pkgr={
            "git": "http://test",
            "ref": "",
            "timeout": 600,
            "patch_file": patch,
            "sdir": "",
            "proj_name": "",
        },
        tiamat={"config": "/pkgr/test"},
    )

    mock_hub.tool.path.clean_path = hub.tool.path.clean_path
    mock_hub.pkgr.init.gather = hub.pkgr.init.gather
    mock_patch = mock.patch("subprocess.run", return_value=mock.MagicMock())
    mock_os = mock.patch("os.makedirs")
    with mock_patch, mock_os:
        ret = mock_hub.pkgr.init.gather()
    if exp_ret:
        assert mock_hub.tiamat.cmd.run.call_args == call(
            ["git", "apply", patch],
            cwd=pathlib.Path("test"),
            fail_on_error=True,
            timeout=600,
        )
        assert mock_hub.tiamat.cmd.run.call_count == 3
    else:
        assert not ret
        assert mock_hub.tiamat.cmd.run.call_count == 2
