import pathlib
from unittest import mock


def test_build(mock_hub, hub, tmp_path):
    """
    Test correct paths and files are created
    when running hub.pkgr.rpm.build
    """
    # set test options

    mock_hub.OPT.pkgr = {
        "sdir": tmp_path / "sdir",
        "system": "rpm",
        "sources": "sources",
        "timeout": 600,
    }
    mock_hub.tool.sdist.extract_sdist.return_value = pathlib.Path(
        mock_hub.OPT.pkgr.sdir, "salt"
    )
    hub.tool.sdist.extract_sdist
    salt_dir = mock_hub.OPT.pkgr.sdir / "salt"
    salt_dir.mkdir()

    tar_file = (
        mock_hub.OPT.pkgr.sdir
        / f"{mock_hub.pkgr.proj_name}-{mock_hub.pkgr.SALT_VERSION}.tar.gz"
    )
    tar_file.open("w").write("")

    run_file = mock_hub.pkgr.CDIR.join("sources", "run.py")
    run_file.write("", ensure=True)
    for fp in ["SOURCES", "SPECS", "BUILD", "SRPMS"]:
        mock_hub.pkgr.BDIR.join(fp)

    proc = mock.MagicMock()
    proc.returncode = 0

    patch_proc = mock.patch("subprocess.run", return_value=proc)
    with patch_proc:
        mock_hub.pkgr.rpm.build = hub.pkgr.rpm.build
        mock_hub.pkgr.rpm.build()
    assert pathlib.Path(mock_hub.pkgr.CDIR, "artifacts").is_dir()
    assert (
        pathlib.Path(mock_hub.pkgr.BDIR, "SPECS", "pkg.spec").read_text()
        == "{ version }"
    )
    assert pathlib.Path(
        mock_hub.pkgr.BDIR,
        "SOURCES",
        mock_hub.pkgr.proj_name + "-" + mock_hub.pkgr.SALT_VERSION + ".tar.gz",
    ).is_file()
