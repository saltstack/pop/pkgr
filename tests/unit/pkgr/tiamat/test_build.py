import pathlib
import subprocess
from unittest import mock

from dict_tools import data


def test_build(mock_hub, hub, tmp_path):
    """
    Test correct paths and files are created
    when running hub.pkgr.tiamat.build
    """
    # set test options
    mock_hub.OPT.pkgr = {
        "sdir": tmp_path / "sdir",
        "system": "tiamat",
        "sources": mock_hub.pkgr.CDIR.join("sources").ensure(dir=True),
        "timeout": 600,
    }
    mock_hub.OPT.pkgr.system = "tiamat"
    mock_hub.OPT.pkgr.sources = mock_hub.pkgr.CDIR.join("sources").ensure(dir=True)
    mock_hub.OPT.pkgr.timeout = 600
    mock_hub.pkgr.SALT_VERSION = "4000"
    mock_hub.pkgr.PKG_VERSION = "99"
    mock_hub.OPT.tiamat = mock.MagicMock()
    mock_hub.OPT.tiamat.onedir = False

    # create test files
    tarball_name = f"{mock_hub.pkgr.proj_name}-{mock_hub.pkgr.SALT_VERSION}-{mock_hub.pkgr.PKG_VERSION}-linux-amd64.tar.gz"
    tar_file = mock_hub.OPT.pkgr.sdir / "salt" / "dist" / tarball_name
    tar_file.parent.mkdir(parents=True)
    tar_file.open("w").write("")
    run_file = mock_hub.pkgr.CDIR.join("sources", "run.py")
    run_file.write("", ensure=True)

    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
    mock_hub.pkgr.tiamat.build = hub.pkgr.tiamat.build
    mock_hub.pkgr.info.host.detect_architecture.return_value = "amd64"
    mock_hub.pkgr.tiamat.build()
    call_args = mock_hub.tiamat.build.builder.call_args_list[0][1]
    assert call_args["directory"] == str(mock_hub.OPT.pkgr.sdir / "salt")
    assert pathlib.Path(
        mock_hub.pkgr.CDIR.join(
            "artifacts",
            mock_hub.pkgr.SALT_VERSION + "-" + mock_hub.pkgr.PKG_VERSION,
            tarball_name,
        )
    ).exists()
