import platform


def detect_architecture(hub):
    """
    Detect architecture on hosts
    """
    arch_map = {
        "arch": {
            "64bit": "amd64",
            "aarch64": "arm64",
            "armv7l": "armv7",
            "32bit": "x86",
        }
    }
    arch = platform.architecture()[0]
    return arch_map["arch"][arch]
