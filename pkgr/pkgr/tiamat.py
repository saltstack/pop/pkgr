import hashlib
import os
import pathlib
import platform
import shutil
import sys
import urllib.request


def __virtual__(hub):
    if sys.platform.startswith("win"):
        required_bins = {"tiamat"}
    else:
        required_bins = {"tar", "tiamat"}
    for rbin in list(required_bins):
        if shutil.which(rbin) is not None:
            required_bins.remove(rbin)
    if required_bins:
        return (
            False,
            f"The following binaries were not found on the system: {', '.join(required_bins)}",
        )
    return True


def build(hub):
    """
    Build the package using tiamat
    """
    # Copy the required files into salt dir
    # build with tiamat with build.conf file
    sources = os.path.join(hub.OPT.pkgr.sdir, "salt")
    for fn in os.listdir(hub.OPT.pkgr.sources):
        full = os.path.join(hub.OPT.pkgr.sources, fn)
        shutil.copy(full, sources)

    os.chdir(sources)

    hub.tiamat.build.builder(
        name=hub.OPT.tiamat.name,
        requirements=hub.OPT.tiamat.requirements,
        sys_site=hub.OPT.tiamat.system_site,
        exclude=hub.OPT.tiamat.exclude,
        directory=os.path.abspath(sources),
        pyinstaller_version=hub.OPT.tiamat.pyinstaller_version,
        pyinstaller_runtime_tmpdir=hub.OPT.tiamat.pyinstaller_runtime_tmpdir,
        datas=hub.OPT.tiamat.datas,
        build=hub.OPT.tiamat.build,
        pkg=hub.OPT.tiamat.pkg,
        onedir=hub.OPT.tiamat.onedir,
        pyenv=hub.OPT.tiamat.pyenv,
        run=hub.OPT.tiamat.run,
        no_clean=hub.OPT.tiamat.no_clean,
        locale_utf8=hub.OPT.tiamat.locale_utf8,
        dependencies=hub.OPT.tiamat.dependencies,
        release=hub.OPT.tiamat.release,
        pkg_tgt=hub.OPT.tiamat.pkg_tgt,
        pkg_builder=hub.OPT.tiamat.pkg_builder,
        srcdir=hub.OPT.tiamat.srcdir,
        system_copy_in=hub.OPT.tiamat.system_copy_in,
        tgt_version=hub.OPT.tiamat.tgt_version,
        venv_plugin=hub.OPT.tiamat.venv_plugin,
        python_bin=hub.OPT.tiamat.python_bin,
        omit=hub.OPT.tiamat.omit,
        pyinstaller_args=hub.OPT.tiamat.pyinstaller_args,
        timeout=hub.OPT.tiamat.timeout,
    )

    # create versioned tarfile
    dist = pathlib.Path("dist").resolve()
    kernel = platform.system().lower()
    arch = hub.pkgr.info.host.detect_architecture()
    archive_name = (
        f"salt-{hub.pkgr.SALT_VERSION}-{hub.pkgr.PKG_VERSION}-{kernel}-{arch}"
    )
    if hub.OPT.tiamat.onedir:
        # This is a onedir, move the run directory into a salt directory
        os.chdir(str(dist))
        # We'll use salt_tmp here as the name of the directory we're moving
        # may be `salt` (hub.OPT.tiamat.name)
        salt_dir = dist / "salt_tmp"
        salt_dir.mkdir()
        # On Windows we use the tiamat.name, other OSes it's always "run"
        tiamat_name = hub.OPT.tiamat.name if sys.platform.startswith("win") else "run"
        shutil.move(str(dist / tiamat_name), str(salt_dir))
        # After we move the directory, rename to the intended name of `salt`
        salt_dir.rename("salt")
        salt_dir = dist / "salt"
    else:
        salt_dir = dist

    if sys.platform.startswith("win"):
        # Download ssm.exe so we can start the minion/master/api as a service
        # This should drop it in a directory named salt for onedir builds and
        # next to the binary for singlebin builds
        src_file = urllib.request.urlopen(hub.OPT.pkgr.ssm_url)
        tgt_file = salt_dir / "ssm.exe"
        with open(tgt_file, "wb") as fh:
            fh.write(src_file.read())

        with open(tgt_file, "rb") as fh:
            bytes = fh.read()
            tgt_file_hash = hashlib.sha256(bytes).hexdigest()

        if tgt_file_hash != hub.OPT.pkgr.ssm_sha256:
            raise Exception(
                "Hash does not match for downloaded file:\n"
                f"\tFile:\t\t{tgt_file}\n"
                f"\tSha256:\t\t{tgt_file_hash}\n"
                f"\tExpected:\t{hub.OPT.pkgr.ssm_sha256}"
            )

        # Create the zipfile for Windows
        archive_name = f"{archive_name}.zip"
        hub.tiamat.cmd.run(
            [
                "powershell",
                "$ProgressPreference='SilentlyContinue'; Compress-Archive",
                "-Path",
                str(salt_dir) if hub.OPT.tiamat.onedir else "*.exe",
                "-DestinationPath",
                archive_name,
            ],
            cwd=str(dist),
            fail_on_error=True,
            timeout=hub.OPT.pkgr.timeout,
        )
    else:
        # Create the tarball for *nix
        archive_name = f"{archive_name}.tar.gz"
        hub.tiamat.cmd.run(
            ["tar", "-czvf", archive_name, "salt"],
            cwd=str(dist),
            fail_on_error=True,
            timeout=hub.OPT.pkgr.timeout,
        )
    artifacts = os.path.join(
        hub.pkgr.CDIR, "artifacts", hub.pkgr.SALT_VERSION + "-" + hub.pkgr.PKG_VERSION
    )
    if not os.path.isdir(artifacts):
        hub.log.debug(f"[pkgr] Making Directory: {artifacts}")
        os.makedirs(artifacts)
    shutil.copy(str(dist / archive_name), artifacts)


def render(hub):
    pass
