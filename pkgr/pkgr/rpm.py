import os
import pathlib
import shutil

import jinja2


def __virtual__(hub):
    required_bins = {"tar", "rpmbuild"}
    for rbin in list(required_bins):
        if shutil.which(rbin) is not None:
            required_bins.remove(rbin)
    if required_bins:
        return (
            False,
            f"The following binaries were not found on the system: {', '.join(required_bins)}",
        )
    return True


def build(hub):
    """
    Build the package!
    """
    # Prep the build tree
    # Copy the sources in
    # place the spec file down
    # run rpmbuild -ba
    proj_version = (
        f"{hub.pkgr.proj_name}-{hub.pkgr.SALT_VERSION.replace('rc', '~rc')}.tar.gz"
    )
    new_dir = hub.tool.sdist.extract_sdist()
    new_dir.rename(new_dir.parent / new_dir.name.replace("rc", "~rc"))
    new_dir = new_dir.parent / new_dir.name.replace("rc", "~rc")
    os.chdir(new_dir.parent)
    hub.tiamat.cmd.run(
        [
            "tar",
            "-acvf",
            proj_version,
            "-C",
            str(new_dir.parent),
            str(new_dir.name),
        ],
        fail_on_error=True,
        timeout=hub.OPT.pkgr.timeout,
    )

    os.chdir(hub.pkgr.CDIR)

    for dir_ in ("SOURCES", "SPECS", "BUILD", "SRPMS"):
        tdir = os.path.join(hub.pkgr.BDIR, dir_)
        os.makedirs(tdir)
    spec = os.path.join(hub.pkgr.BDIR, "SPECS", "pkg.spec")
    sources = os.path.join(hub.pkgr.BDIR, "SOURCES")
    rpms = os.path.join(hub.pkgr.BDIR, "RPMS")
    srpms = os.path.join(hub.pkgr.BDIR, "SRPMS")
    shutil.copy(new_dir.parent / proj_version, sources)
    for fn in os.listdir(hub.OPT.pkgr.sources):
        full = os.path.join(hub.OPT.pkgr.sources, fn)
        shutil.copy(full, sources)
    with open(spec, "w+") as wfh:
        wfh.write(hub.pkgr.SPEC)
    hub.tiamat.cmd.run(
        ["rpmbuild", "--define", f"_topdir {hub.pkgr.BDIR}", "-ba", spec],
        fail_on_error=True,
        timeout=hub.OPT.pkgr.timeout,
    )
    artifacts = os.path.join(hub.pkgr.CDIR, "artifacts")
    if not os.path.isdir(artifacts):
        hub.log.debug(f"[pkgr] Making Directory: {artifacts}")
        os.mkdir(artifacts)
    for root, dirs, files in os.walk(rpms):
        for fn in files:
            if fn.endswith("rpm"):
                full = os.path.join(root, fn)
                shutil.copy(full, artifacts)
    for root, dirs, files in os.walk(srpms):
        for fn in files:
            if fn.endswith("rpm"):
                full = os.path.join(root, fn)
                shutil.copy(full, artifacts)


def render(hub):
    """
    Render the spec file
    """
    opts = dict(hub.OPT.pkgr)
    opts["version"] = hub.pkgr.SALT_VERSION.replace("rc", "~rc")
    opts["pkg_version"] = hub.pkgr.PKG_VERSION
    with open(hub.OPT.pkgr.spec) as rfh:
        data = rfh.read()
    template = jinja2.Template(data)
    hub.pkgr.SPEC = template.render(**opts)
