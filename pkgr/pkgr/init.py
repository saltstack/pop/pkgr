# Prepare primary sources as tarball
# Prep temp rpm build tree
# Run the spec file through jinja
# Save output into rpm build tree
# Copy sources into rpm build tree
# build rpm
import os
import pathlib
import subprocess
import tempfile


def __init__(hub):
    for sub in ["tiamat", "tool"]:
        hub.pop.sub.add(dyne_name=sub)
    hub.pop.sub.load_subdirs(hub.pkgr)
    hub.pkgr.BDIR = tempfile.mkdtemp()
    hub.pkgr.CDIR = os.getcwd()


def cli(hub):
    hub.pop.config.load(["pkgr", "tiamat"], cli="pkgr")
    hub.pkgr.init.gather()
    getattr(hub, f"pkgr.{hub.OPT.pkgr.system}.render", lambda: 0)()
    getattr(hub, f"pkgr.{hub.OPT.pkgr.system}.build")()


def gather(hub):
    hub.pkgr.ref.init.gather()
    git = hub.OPT.pkgr.git
    if hub.OPT.pkgr.proj_name:
        hub.pkgr.proj_name = hub.OPT.pkgr.proj_name
    else:
        hub.pkgr.proj_name = git.rsplit("/", 1)[-1].split(".")[0]
    os.makedirs(hub.OPT.pkgr.sdir, exist_ok=True)
    if not os.path.exists(os.path.join(hub.OPT.pkgr.sdir, "salt")):
        hub.tiamat.cmd.run(
            ["git", "clone", git, hub.pkgr.proj_name],
            cwd=hub.OPT.pkgr.sdir,
            fail_on_error=True,
            timeout=hub.OPT.pkgr.timeout,
        )

    if hub.pkgr.GIT_REF:
        try:
            # Check to see if the git tag with the build number
            # exists or not.
            hub.tiamat.cmd.run(
                ["git", "checkout", hub.pkgr.FULL_GIT_TAG or hub.pkgr.GIT_REF],
                cwd=pathlib.Path(hub.OPT.pkgr.sdir) / hub.pkgr.proj_name,
                fail_on_error=True,
                timeout=hub.OPT.pkgr.timeout,
            )
        except subprocess.CalledProcessError as err:
            # The full git tag with the package build number does not
            # exist. Using just the git tag without the build number
            hub.tiamat.cmd.run(
                ["git", "checkout", hub.pkgr.GIT_REF],
                cwd=pathlib.Path(hub.OPT.pkgr.sdir) / hub.pkgr.proj_name,
                fail_on_error=True,
                timeout=hub.OPT.pkgr.timeout,
            )

    patch_file = hub.OPT.pkgr.patch_file
    if patch_file:
        if not pathlib.Path(patch_file).is_absolute():
            hub.log.error(f"Patch file {patch_file} must be an absolute path")
            return False
        config = pathlib.Path(hub.OPT.tiamat.config)
        if config.is_absolute():
            proj_root = pathlib.Path(os.sep + config.parts[1])
        else:
            proj_root = pathlib.Path().cwd()
        if not hub.tool.path.clean_path(proj_root, patch_file):
            hub.log.error(f"The patch file {patch_file} path is not valid")
            return False
        hub.tiamat.cmd.run(
            ["git", "apply", patch_file],
            cwd=pathlib.Path(hub.OPT.pkgr.sdir) / hub.pkgr.proj_name,
            fail_on_error=True,
            timeout=hub.OPT.pkgr.timeout,
        )

    hub.pkgr.ver.init.gather()
