import sys
import tempfile

CLI_CONFIG = {
    "spec": {},
    "sources": {},
    "git": {},
    "ref": {},
    "pkg_version": {},
    "system": {},
    "config": {"options": ["-c"], "source": "tiamat"},
    "sdir": {},
    "epoch": {},
    "timeout": {"type": int},
    "patch_file": {},
}
CONFIG = {
    "ssm_url": {
        "default": "https://repo.saltproject.io/windows/dependencies/64/ssm-2.24-103-gdee49fc.exe",
        "help": "Url for the Salt Service Manager (ssm.exe) which manages services on Windows",
    },
    "ssm_sha256": {
        "default": "7ebdd1c146b13b9d2882f5eee9d7e772b57d1ef3ba3b22cf43232a34559a9dbc",
        "help": "The sha-256 hash for the ssm.exe binary",
    },
    "spec": {
        "default": None,
        "help": "The location of the spec file to use when using the rpm system",
    },
    "sources": {
        "default": "",
        "help": "a directory containing extra sources files when using the rpm system",
    },
    "debian_dir": {
        "default": "",
        "help": "the debian directory to copy into the project when using the deb system",
    },
    "git": {
        "default": None,
        "help": "A git location to gather the project from",
    },
    "ref": {
        "default": None,
        "help": "The name of the git branch, commit or tag to checkout",
    },
    "system": {
        "default": "tiamat",
        "help": "Choose what type of system to build to, rpm or deb",
    },
    "ver": {
        "default": "salt",
        "help": (
            "Choose what version gathering system to use to dynamically determine the "
            "version number to apply to the package. If instead you wish to statically "
            "set the version number simply pass the desired version number"
        ),
    },
    "pkg_version": {
        "default": None,
        "help": (
            "The package version to use(this is not the project version). Defaults to: 1"
        ),
    },
    "python": {
        "default": sys.executable,
        "help": "The python binary to use.",
    },
    "config": {
        "default": "",
        "help": "The location of the configuration file",
    },
    "sdir": {
        "default": tempfile.mkdtemp(),
        "help": "The source directory where a git clone of salt exists. "
        "By default it will be a temporary directory",
    },
    "timeout": {
        "default": 600,
        "type": int,
        "help": "The length of time in seconds to run commands",
    },
    "proj_name": {
        "default": "",
        "help": "The name of the project you are building",
    },
    "epoch": {
        "default": None,
        "help": "The epoch to include in the version",
    },
}
SUBCOMMANDS = {}
DYNE = {
    "pkgr": ["pkgr"],
    "tool": ["tool"],
}
