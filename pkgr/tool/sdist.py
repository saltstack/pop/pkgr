import pathlib
import shutil
import tarfile


def extract_sdist(hub):
    """
    Extract and use the sdist generated
    """
    sdir = pathlib.Path(hub.OPT.pkgr.sdir)
    saltd = sdir / "salt"
    sdist = [e for e in saltd.joinpath("dist").iterdir()][0]

    with tarfile.open(sdist) as fp:
        fp.extractall(path=sdir)

    shutil.rmtree(saltd)

    saltv = [e for e in sdir.iterdir()][0]
    return saltv
